<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
  
  <link rel="stylesheet" href="stylee2.css">
  
  <title> WEB PROFIL KIQI</title>
</head>
<body>
  <div class="wrapper">
    <!-- nav -->
    <nav>
      <div class="logo">
        <span class=""></span>
        <a href="=#"><img src="logo uad.png" class="logo" width="100" height="100" ></a>
        
      </div>
      </br></br>
      <ul>
        
        <li><b><a href="#title">Home</a></b></li>
        <!--<li><b><a href="#login">Login</a></b></li>-->
        <li><b><a href="#about">About</a></b></li>
        <li><b><a href="#photos">fotografi</a></b></li>
        <li><b><a href="#contact">Contact</a></b></li>
        <li><b><a href="#logout">logout</a></b></li>
      </ul>
    </nav>
    <!-- header -->
    <header id="title">
        <div class="content">
        <div class="textBox">
        </br></br>
        <h2>Website Pertama <br><span>Kiqi </span></h2>
        <p>HTML atau Hypertext Markup Language adalah bahasa markup yang digunakan untuk membuat struktur halaman website agar dapat ditampilkan pada web browser. Jadi, HTML dapat dianalogikan sebagai pondasi awal dalam menyusun kerangka halaman web secara terstruktur sebelum membahas terkait tampilan desain dan sisi fungsionalitas.
      </p>
      </br>
      <a href="#about">pelajari lebih lanjut</a>
    </div>
    <div class="imgBox">
      <img src="logo uad.png" class="uad">
    </div>
  </div>
  <ul class="thumb">
    <li><img src="profil hitam.png" onclick="imgSlider('foto bersama.jpg') ;changeBgColor('white')" ></li>
    <li><img src="profil red.png" onclick="imgSlider('foto bersama2.jpg');changeBgColor('#E8F9FD')"></li>
    <li><img src="profile kuning.png" onclick="imgSlider('foto bersama 3.jpg');changeBgColor('#112B3C')"></li>
  </ul>
  <ul class="sci">
    <li><a href="https://m.facebook.com/"><img src="facebook.png"></a></li>
    <li><a href="https://mobile.twitter.com/home"><img src="twitter.png"></a></li>
    <li><a href="https://instagram.com/gdobleodjob?igshid=YmMyMTA2M2Y="><img src="instagram.png"></a></li>
  </ul>
    </header>


    <!-- quote section -->
    <section class="quote">
      <p>"Kesabaran adalah akhlak mulia, yang dengannya setiap orang dapat menghalau segala rintangan." - Imam Syafi'i</p>
    </section>


    <!-- main -->
    <main>
      <!-- about -->
      

       <section class="about"></section>
      <article id="about">
        <aside>
          <h2>Profile</h2>
          </br></br>
      </aside>
    
        <p align="right"></br></br></br>Nama saya adalah kiqi safi'ah m biasa dipanggil kiqi, saya berusia 18 tahun dan saat ini menempuh penididkan di Universitas Ahmad Dahlan Yogyakarta pada semester 2. Asal saya dari Sleman yogyakarta tepatnya pada daerah ngemplak sleman yogyakarta. hobi saya salah satunya adalah fotografi, oleh karena itu didalam web profil pertama saya ini terdapat beberapa portofolio dari hasil camera saya.</p>
        <img src="fotoprofile.jpeg" alt="Profile" align="left" width="500" height="550">
       <!-- <aside>
          <h2>Profile</h2>-->
      </aside>
      </article>
      </section>
    
     

      <!-- images -->
      <section id="photos"></section>
     <section class="photos">
        <img src="camera1.jpeg" alt="taman budaya" align="left" width="100" height="200">

        <img src="camera2.jpeg" alt="taman budaya" align="right" width="100" height="200">

        <img src="camera3.jpeg" alt="taman budaya" align="right" width="100" height="200">

        <img src="camera4.jpeg" alt="taman budaya" align="left" width="100" height="200">
      </section>
    </main>

    <!-- quote section -->
    <section class="quote">
      <p>"Ini adalah beberapa tangkapan camera pada pameran taman budaya yogyakarta"</p>
    </section>

    <section id="contact">
      <h2>Contact Us</h2>

      <form name="form1" method="post" action="proses.php">
        <label for="name">Name</label>
        <input name="nama" type="text" id="nama">
        <!--<input id="name" type="text">-->

        <label for="email">Email</label>
        <input name="email" type="text" id="email">
        <!--<input id="email" type="email">-->

        <label for="alamat">ALAMAT</label>
        <input name="alamat" type="text" id="alamat">
        <!--<input id="email" type="email">-->


        <label for="tanggapan">Tanggapan</label>
        <input name="tanggapan" type="text" id="tanggapan">
        <!--<input id="email" type="email">-->

        <label for="comments">Comments</label>
        <textarea id="comments"></textarea>
        <!--<input name="comen" type="text" id="comments">-->
        <input class="button" type="submit" value="Submit">

        <div align=""><strong><a href="lihat.php"></strong></div>

      </form>
    </section>

    <!--<div align="center"><strong><a href="lihat.php">Lihat strukt pembayaran</a></strong></div>-->

    <!-- footer -->
    <footer id="footer">
      <p>tamanbuadaya &copy; 2022</p>
    </footer>
  </div>

  <!-- scripts -->
  <script src="app.js"></script>
</body>
</html>